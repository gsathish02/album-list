//
//  AlbumListTests.swift
//  AlbumListTests
//
//  Created by Sathish Govindaraj on 9/17/20.
//  Copyright © 2020 Nike Inc,. All rights reserved.
//

import XCTest

@testable import AlbumList

class AlbumListTests: XCTestCase {
    
    var viewControllerTest = ViewController()
    
    override func setUp() {
        super.setUp()
        self.viewControllerTest.loadView()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testHasATableView() {
        XCTAssertNotNil(self.viewControllerTest.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(self.viewControllerTest.tableView.delegate)
    }

    func testTableViewHasDataSource() {
        XCTAssertNotNil(viewControllerTest.tableView.dataSource)
    }

    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(viewControllerTest.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(viewControllerTest.responds(to: #selector(viewControllerTest.tableView(_:didSelectRowAt:))))
    }
    
    func testNumberofRows() {
        XCTAssertTrue(viewControllerTest.tableView.numberOfRows(inSection: 0) == 0)
    }
}
