//
//  FeedResult.swift
//  AlbumList
//
//  Created by Sathish Govindaraj on 9/17/20.
//  Copyright © 2020 Nike Inc,. All rights reserved.
//

import Foundation

struct FeedResult: Decodable {
    
    var name: String
    var artistName: String
    var artImageUrlString: String
    var copyright : String
    var releaseDate: String
    var artistUrl: String
    var genres: [genresResult]
   
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        artistName = try values.decode(String.self, forKey: .artistName)
        artImageUrlString = try values.decode(String.self, forKey: .artImageUrlString)
        copyright = try values.decode(String.self, forKey: .copyright)
        releaseDate = try values.decode(String.self, forKey: .releaseDate)
        artistUrl = try values.decode(String.self, forKey: .artistUrl)
        genres = try values.decode([genresResult].self, forKey: .genres)
    }
    
    private enum CodingKeys: String, CodingKey {
        case artImageUrlString = "artworkUrl100"
        case name
        case artistName
        case copyright
        case releaseDate
        case artistUrl = "url"
        case genres
    }
}

struct Feed: Decodable {
    
    var results: Result
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        results = try values.decode(Result.self, forKey: .results)
    }
    
    private enum CodingKeys: String, CodingKey {
        case results = "feed"
    }
}

struct Result: Decodable {
    
    var feedResults: [FeedResult]
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        feedResults = try values.decode([FeedResult].self, forKey: .feedResults)
    }
    
    private enum CodingKeys: String, CodingKey {
        case feedResults = "results"
    }
}

struct genresResult: Decodable {
    
    var geresName: String
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        geresName = try values.decode(String.self, forKey: .name)
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
    }
}
