//
//  DetailViewController.swift
//  AlbumList
//
//  Created by Sathish Govindaraj on 9/17/20.
//  Copyright © 2020 Nike Inc,. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var result: FeedResult?
    var safeArea = UILayoutGuide()
    let albumArtImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        safeArea = view.layoutMarginsGuide
        self.title = "Album Info"
        setupUIComponents()
    }
    
    func setupUIComponents() {
        view.backgroundColor = .white
        
        //Stack View
        let stackView   = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 32
        
        //Image View
        let imageView = UIImageView()
        if let urlString = result?.artImageUrlString, let imageURL = URL(string: urlString) {
            DispatchQueue.global().async { [] in
                if let data = try? Data(contentsOf: imageURL) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                           imageView.image = image
                        }
                    }
                }
            }
        }
        stackView.addArrangedSubview(imageView)
        
        //Text Labels
        if let result = result {
            if result.genres.count != 0 {
                //First index always has actual genre 
                let genresLabel = getLabel(string: result.genres[0].geresName)
                stackView.addArrangedSubview(genresLabel)
            }
        }
        if let rdLabel = result?.releaseDate {
            let releaseInfoLabel = getLabel(string: rdLabel)
            stackView.addArrangedSubview(releaseInfoLabel)
        }
        if let crLabel = result?.copyright {
            let copyRightsLabel = getLabel(string: crLabel)
            stackView.addArrangedSubview(copyRightsLabel)
        }
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(stackView)
        
        //Button - Show Album
        let button = UIButton(frame: CGRect(x: 20, y: self.view.frame.height - 40 , width: self.view.frame.width - 40 , height: 20))
        button.setTitle("Show Album", for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(UIColor.blue, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        self.view.addSubview(button)
        
        //Constraints
        button.anchor(top: stackView.bottomAnchor,
                      topConstant: 20,
                      bottom: view.safeAreaLayoutGuide.bottomAnchor,
                      bottomConstant: 20,
                      leading: view.safeAreaLayoutGuide.leadingAnchor,
                      leadingConstant: 20,
                      trailing: view.safeAreaLayoutGuide.trailingAnchor,
                      trailingConstant: 20)
        
       
        stackView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         topConstant: 25,
                         bottom: button.topAnchor,
                         bottomConstant: 20,
                         leading: view.safeAreaLayoutGuide.leadingAnchor,
                         leadingConstant: 30,
                         trailing: view.safeAreaLayoutGuide.trailingAnchor,
                         trailingConstant: 30)
    }
    
    private func getLabel(string: String, weigth: UIFont.Weight = .regular, fontSize: CGFloat = 17, lineHeightMultiple: CGFloat = 22/17) -> UILabel {

        let label = UILabel()
        
        // Attributes
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize, weight: weigth)]

        // Paragraph
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .center
    
        // Config. attributed string
        let attrString = NSMutableAttributedString(string: string)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrString.length))
        attrString.addAttributes(attributes, range: NSRange(location: 0, length: attrString.length))

        // Config. Label
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.attributedText = attrString

        return label
    }
    
    @objc func buttonTapped() {
        if let urlString = result?.artistUrl, let url = URL(string: urlString) {
            UIApplication.shared.open(url)
        }
    }
}

