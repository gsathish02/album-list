//
//  ViewController.swift
//  AlbumList
//
//  Created by Sathish Govindaraj on 9/17/20.
//  Copyright © 2020 Nike Inc,. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let tableView = UITableView()
    var safeArea = UILayoutGuide()
    let CellIdentifier = "cell"
    var totalResult: [FeedResult] = []
    var cache: NSCache<AnyObject, AnyObject>?
    
    override func loadView() {
        super.loadView()
        initialSetup()
        setupTableView()
        fetchFeedResults()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    func initialSetup() {
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        self.title = "Albums"
        self.cache = NSCache()
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 100
    }
    
    func fetchFeedResults() {
        let urlString = "https://rss.itunes.apple.com/api/v1/us/apple-music/coming-soon/all/100/explicit.json"
        if let url = URL(string: urlString) {
           URLSession.shared.dataTask(with: url) { data, response, error in
              if let data = data {
                  do {
                    let res = try JSONDecoder().decode(Feed.self, from: data)
                    self.totalResult = res.results.feedResults
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.tableView.reloadData()
                    }
                  } catch let error {
                     print(error)
                  }
              }
           }.resume()
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: CellIdentifier)
        cell.textLabel?.text = totalResult[indexPath.row].name
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        
        cell.detailTextLabel?.text = totalResult[indexPath.row].artistName
        cell.imageView?.image =  #imageLiteral(resourceName: "placeHolder") // Placeholder
        
        if (self.cache?.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
            // Use cache
            cell.imageView?.image = self.cache?.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
        } else {
            if let imageURL = URL(string: totalResult[indexPath.row].artImageUrlString) {
               DispatchQueue.global().async { [weak self] in
                   if let data = try? Data(contentsOf: imageURL) {
                       if let image = UIImage(data: data) {
                           DispatchQueue.main.async {
                            cell.imageView?.image = image
                            self?.cache?.setObject(image, forKey: indexPath.row as AnyObject)
                           }
                       }
                   }
               }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController()
        detailVC.result = totalResult[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
